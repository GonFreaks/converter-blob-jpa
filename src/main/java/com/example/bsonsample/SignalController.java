package com.example.bsonsample;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.v3.oas.annotations.Operation;

import java.util.List;

@RestController
@RequestMapping("/signal")
@Tag(name = "signal controller", description = "${test2}")
public class SignalController {

    @Autowired
    private SignalRepository repository;

    @GetMapping
    @Operation(description = "${test}")
    public List<Signal> findAll(){
        return repository.findAll();
    }

    @PostMapping
    @Operation(description = "${test}")
    public Signal create(@Parameter(name = "test", description = "${test2}") @RequestBody Signal signal) {
        return repository.save(signal);
    }
}
