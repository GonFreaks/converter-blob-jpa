package com.example.bsonsample;

import javax.persistence.AttributeConverter;
import java.util.Arrays;

public class StringToByteArrayConverter implements AttributeConverter<String, byte[]> {


    @Override
    public String convertToEntityAttribute(byte[] bytes) {
        return bytes != null ? new String(bytes) : null;
    }

    @Override
    public byte[] convertToDatabaseColumn(String s) {
        return s.getBytes();
    }
}
