package com.example.bsonsample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BsonSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(BsonSampleApplication.class, args);
    }

}
