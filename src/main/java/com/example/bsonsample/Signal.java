package com.example.bsonsample;

import javax.persistence.*;
import java.util.UUID;

@Entity
public class Signal {

    @Id
    private String id;

    @Lob
    @Column(columnDefinition = "BLOB")
    @Convert(converter = StringToByteArrayConverter.class)
    private String blobData;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBlobData() {
        return blobData;
    }

    public void setBlobData(String blobData) {
        this.blobData = blobData;
    }

    @PrePersist
    public void autofill() {
        if (this.getId() == null) {
            this.setId(UUID.randomUUID().toString());
        }
    }
}
